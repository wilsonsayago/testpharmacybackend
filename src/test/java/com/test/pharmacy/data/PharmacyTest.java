package com.test.pharmacy.data;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PharmacyTest {

    @Test
    void setDataByMap() {
        Pharmacy pharmacy = new Pharmacy();
        Pharmacy pharmacyTest = new Pharmacy();
        pharmacy.setLocalDirection("test");
        pharmacy.setLocalLat("0");
        pharmacy.setLocalLng("0");
        pharmacy.setLocalName("test");
        pharmacy.setLocalPhone("12345");

        pharmacyTest.setDataByMap("local_nombre", "test");
        pharmacyTest.setDataByMap("local_direccion", "test");
        pharmacyTest.setDataByMap("local_telefono", "12345");
        pharmacyTest.setDataByMap("local_lat", "0");
        pharmacyTest.setDataByMap("local_lng", "0");

        assertEquals(pharmacy.getLocalDirection(), pharmacyTest.getLocalDirection());
        assertEquals(pharmacy.getLocalLat(), pharmacyTest.getLocalLat());
        assertEquals(pharmacy.getLocalLng(), pharmacyTest.getLocalLng());
        assertEquals(pharmacy.getLocalName(), pharmacyTest.getLocalName());
        assertEquals(pharmacy.getLocalPhone(), pharmacyTest.getLocalPhone());

    }
}