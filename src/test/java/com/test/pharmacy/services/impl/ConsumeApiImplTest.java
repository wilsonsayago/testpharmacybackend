package com.test.pharmacy.services.impl;

import com.test.pharmacy.controllers.CommunesController;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.ConfigFileApplicationContextInitializer;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

class ConsumeApiImplTest {

    @InjectMocks
    private ConsumeApiImpl consumeApi;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ReflectionTestUtils.setField(consumeApi, "regId", "7");
    }


    @Test
    void getAllCommunes() {
        ReflectionTestUtils.setField(consumeApi, "urlPostCommunes", "https://midastest.minsal.cl/farmacias/maps/index.php/utilidades/maps_obtener_comunas_por_regiones");
        String response = consumeApi.getAllCommunes();
        assertNotEquals("", response);

        ReflectionTestUtils.setField(consumeApi, "urlPostCommunes", null);
        response = consumeApi.getAllCommunes();
        assertEquals("", response);
    }

    @Test
    void getAllPharmacy() {
        ReflectionTestUtils.setField(consumeApi, "urlGetPharmacy", "https://farmanet.minsal.cl/maps/index.php/ws/getLocalesRegion?id_region={fkRegion}");

        List<Map<String, Object>> response = consumeApi.getAllPharmacy();
        assertNotEquals(response.size(), 0);

        ReflectionTestUtils.setField(consumeApi, "urlGetPharmacy", "");
        response = consumeApi.getAllPharmacy();
        assertEquals(response.size(), 0);
    }
}