package com.test.pharmacy.controllers;

import com.test.pharmacy.services.impl.ConsumeApiImpl;
import com.test.pharmacy.services.impl.FilterPharmacyImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class PharmacyControllerTest {

    @InjectMocks
    private PharmacyController pharmacyController;

    @Mock
    private ConsumeApiImpl consumeApi;

    @Mock
    private FilterPharmacyImpl filterPharmacy;

    private MockMvc mockMvc;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders
                .standaloneSetup(pharmacyController)
                .alwaysDo(MockMvcResultHandlers.print())
                .build();
    }

    @Test
    void getAll() throws Exception {
        List<Map<String, Object>> pharmacies = new ArrayList<>();
        when(consumeApi.getAllPharmacy()).thenReturn(pharmacies);
        when(filterPharmacy.getAllPharmaciesByKeyValue(pharmacies, FilterPharmacyImpl.filterByCommune, "0")).thenReturn(new ArrayList<Map<String, Object>>());
        when(filterPharmacy.getAllPharmaciesByKeyValue(pharmacies, FilterPharmacyImpl.filterByLocal, "test")).thenReturn(new ArrayList<Map<String, Object>>());

        mockMvc.perform(MockMvcRequestBuilders.get("/pharmacy?communeId=0&local=test"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.pharmacies").value(pharmacies));
    }
}