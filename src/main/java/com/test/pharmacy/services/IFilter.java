package com.test.pharmacy.services;

import java.util.List;
import java.util.Map;

public interface IFilter {

    List<Map<String, Object>> getAllPharmaciesByKeyValue(List<Map<String, Object>> pharmacies, String key, String value);
    List<Map<String, Object>> getAllPharmaciesOnTurn(List<Map<String, Object>> pharmacies);
}
