package com.test.pharmacy.services;

import java.util.List;
import java.util.Map;

public interface IConsumeApi {

    String getAllCommunes();
    List<Map<String, Object>> getAllPharmacy();
}
