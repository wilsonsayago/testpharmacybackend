package com.test.pharmacy.services.impl;

import com.test.pharmacy.services.IConsumeApi;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@Service
public class ConsumeApiImpl implements IConsumeApi {

    private static final Logger logger = LoggerFactory.getLogger(ConsumeApiImpl.class);

    @Value("${regId}")
    private String regId = null;

    @Value("${url.post.communes}")
    private String urlPostCommunes = null;

    @Value("${url.get.pharmacy}")
    private String urlGetPharmacy = null;

    @Override
    public String getAllCommunes() {
        logger.info("Get All Communes by Call Api midatest.misal.cl");
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
        map.add("reg_id", regId);
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);
        try {
            return restTemplate.postForEntity(urlPostCommunes, request, String.class).getBody();
        } catch (Exception e) {
            logger.error(e.getMessage());
            return "";
        }
    }

    @Override
    public List<Map<String, Object>> getAllPharmacy() {
        logger.info("Get all pharmacies by call api farmanet.minsal.cl");
        RestTemplate restTemplate = new RestTemplate();
        List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        converter.setSupportedMediaTypes(Collections.singletonList(MediaType.ALL));
        messageConverters.add(converter);
        restTemplate.setMessageConverters(messageConverters);
        try {
            return restTemplate.getForObject(urlGetPharmacy, List.class, regId);
        } catch (Exception e) {
            logger.error(e.getMessage());
            return new ArrayList<>();
        }
    }
}
