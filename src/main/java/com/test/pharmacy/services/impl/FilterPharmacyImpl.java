package com.test.pharmacy.services.impl;

import com.test.pharmacy.services.IFilter;
import org.springframework.stereotype.Service;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

@Service
public class FilterPharmacyImpl implements IFilter {

    public final static String filterByCommune = "fk_comuna";
    public final static String filterByLocal = "local_nombre";
    public final static String filterByTimeOpen = "funcionamiento_hora_apertura";
    public final static String filterByTimeClose = "funcionamiento_hora_cierre";

    public FilterPharmacyImpl() {
    }

    @Override
    public List<Map<String, Object>> getAllPharmaciesByKeyValue(List<Map<String, Object>> pharmacies, String key, String value) {
        if(value == null) return null;
        return pharmacies.stream().filter((p) -> p.entrySet().stream().anyMatch(p2 ->
                    p2.getKey().equals(key)
                    && p2.getValue().toString().toLowerCase().contains(value.toLowerCase()))).collect(Collectors.toList());
    }

    @Override
    public List<Map<String, Object>> getAllPharmaciesOnTurn(List<Map<String, Object>> pharmacies) {
        LocalTime timeNow = LocalTime.now();
        AtomicReference<Boolean> afterOpen = new AtomicReference<>(false);
        AtomicReference<Boolean> beforeClosed = new AtomicReference<>(false);
        return pharmacies.stream().filter((p) -> {
            afterOpen.set(false);
            beforeClosed.set(false);
            return p.entrySet().stream().anyMatch(p2 -> {
                if (p2.getKey().equals(filterByTimeOpen)) {
                    LocalTime time = LocalTime.parse(p2.getValue().toString().split(" ")[0], DateTimeFormatter.ISO_TIME);
                    afterOpen.set(timeNow.isAfter(time));
                } else if (p2.getKey().equals(filterByTimeClose)) {
                    LocalTime time = LocalTime.parse(p2.getValue().toString().split(" ")[0], DateTimeFormatter.ISO_TIME);
                    beforeClosed.set(timeNow.isBefore(time));
                }
                if (afterOpen.get() && beforeClosed.get()) {
                    return true;
                }
                return false;
            });
        }).collect(Collectors.toList());
    }
}
