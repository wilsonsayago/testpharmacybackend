package com.test.pharmacy.data;

import java.io.Serializable;

public class Pharmacy implements Serializable {

    private static final long serialVersionUID = 1L;

    private String localName;
    private String localDirection;
    private String localPhone;
    private String localLat;
    private String localLng;

    public Pharmacy() {
    }

    public String getLocalName() {
        return localName;
    }

    public void setLocalName(String localName) {
        this.localName = localName;
    }

    public String getLocalDirection() {
        return localDirection;
    }

    public void setLocalDirection(String localDirection) {
        this.localDirection = localDirection;
    }

    public String getLocalPhone() {
        return localPhone;
    }

    public void setLocalPhone(String localPhone) {
        this.localPhone = localPhone;
    }

    public String getLocalLat() {
        return localLat;
    }

    public void setLocalLat(String localLat) {
        this.localLat = localLat;
    }

    public String getLocalLng() {
        return localLng;
    }

    public void setLocalLng(String localLng) {
        this.localLng = localLng;
    }

    public void setDataByMap(String key, Object value) {
        switch (key) {
            case "local_nombre":
                this.localName = (String) value;
            case "local_direccion":
                this.localDirection = (String) value;
            case "local_telefono":
                this.localPhone = (String) value;
            case "local_lat":
                this.localLat = (String) value;
            case "local_lng":
                this.localLng = (String) value;
        }
    }
}
