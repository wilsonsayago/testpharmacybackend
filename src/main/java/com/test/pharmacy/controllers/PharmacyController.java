package com.test.pharmacy.controllers;

import com.test.pharmacy.data.Pharmacy;
import com.test.pharmacy.services.impl.ConsumeApiImpl;
import com.test.pharmacy.services.impl.FilterPharmacyImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "/pharmacy")
@CrossOrigin(origins = "http://localhost:4200")
public class PharmacyController {

    private static final Logger logger = LoggerFactory.getLogger(PharmacyController.class);

    @Autowired
    private FilterPharmacyImpl filterPharmacy = null;

    @Autowired
    private ConsumeApiImpl consumeApi = null;

    @GetMapping
    public ResponseEntity<Map<String, Object>> getAll(@RequestParam(value = "communeId", required = false) Integer communeId,
                                                      @RequestParam(value = "local", required = false) String local){
        logger.info("get all pharmacy");
        Map<String, Object> response = new HashMap<>();
        List<String> errors = new ArrayList<>();
        try {
            List<Map<String, Object>> pharmacies = consumeApi.getAllPharmacy();
            if (communeId != null) {
                logger.info("filtering by commune");
                pharmacies = filterPharmacy.getAllPharmaciesByKeyValue(pharmacies, FilterPharmacyImpl.filterByCommune, communeId.toString());
            }
            if (local != null) {
                logger.info("filtering by local");
                pharmacies = filterPharmacy.getAllPharmaciesByKeyValue(pharmacies, FilterPharmacyImpl.filterByLocal, local);
            }
            pharmacies = filterPharmacy.getAllPharmaciesOnTurn(pharmacies);
            response.put("pharmacies",
                pharmacies.stream().map((p) -> {
                    Pharmacy pharmacy = new Pharmacy();
                    p.entrySet().stream().forEach(p2 ->
                            pharmacy.setDataByMap(p2.getKey(), p2.getValue()));
                    return pharmacy;
                }).collect(Collectors.toList())
            );
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            logger.error(e.getMessage());
            response.put("message", errors);
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
