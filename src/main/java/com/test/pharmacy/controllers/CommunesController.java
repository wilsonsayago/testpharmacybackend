package com.test.pharmacy.controllers;

import com.test.pharmacy.services.impl.ConsumeApiImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(path = "/communes")
@CrossOrigin(origins = "http://localhost:4200")
public class CommunesController {

    private static final Logger logger = LoggerFactory.getLogger(PharmacyController.class);

    @Autowired
    private ConsumeApiImpl consumeApi;

    @GetMapping
    public ResponseEntity<Map<String, Object>> getAll(){
        logger.info("get all communes");
        Map<String, Object> response = new HashMap<>();
        List<String> errors = new ArrayList<>();
        try {
            response.put("communes", consumeApi.getAllCommunes());
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            logger.error(e.getMessage());
            response.put("message", errors);
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
