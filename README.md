# Getting Started

# Pharmacy Test

### Overview
This is a demo application of a spring boot backend API, and It exposes two REST endpoints, the first allows to get the list of communes, and also another endpoint get all pharmacies with a optional communeId and local to filtering.

* The application does not uses any user and/or password in order to authenticate.
* By default, consume two apis:
  * https://midastest.minsal.cl/farmacias/maps/index.php/utilidades/maps_obtener_comunas_por_regiones
  * https://farmanet.minsal.cl/maps/index.php/ws/getLocalesRegion  

### Running your service

```
$ mvn clean install
$ java -jar target/pharmacy-0.0.1-SNAPSHOT.jar
```
After that, your application should be up and running on [localhost:8080](http://localhost:8080/)

### Tech stack
This is a quick list of the technologies and dependencies used to implement this demo:
* Java, version 1.8
* Apache Tomcat embedded
* Spring Boot, version 2.3.3.RELEASE
    * spring-boot-starter-web, version 2.3.3.RELEASE
    * Spring-boot-starter-test, version 2.3.3.RELEASE
* jackson-databind, version 2.11.2
* springfox-swagger2, version 2.9.2
* springfox-swagger-ui, version 2.9.2

### API documentation
As this demo application uses **Swagger** and **Swagger-ui,** you can easilly view and try the REST endpoints exposed by this backend API.

With the application up and running, you can go to the swagger-ui url in your web browser, by default:
* [Swagger-ui: test](http://localhost:8080/swagger-ui.html#)

The backend API exposes endpoints:
* [GET /communes](http://localhost:8080/communes)
* [GET /pharmacy](http://localhost:8080/pharmacy)
* [GET /pharmacy_by_commune](http://localhost:8080/pharmacy?communeId=)
* [GET /pharmacy_by_local](http://localhost:8080/pharmacy?local=)
